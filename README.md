

## Description

my reponse to this https://github.com/fulll/hiring/blob/master/Backend/ddd-and-cqrs-intermediare-senior.md

## requirements
- surreal DB for the command line tool
- export SURREAL_DB_PASS="passTGSBZR"
- export SURREAL_DB_URL="wss://digital-kin-tes-06ag3efvtds570dkhbarcj8f1s.aws-euw1.surreal.cloud"


## Project setup/ standard commands

```bash
$ npm install
$ npm run lint # lint with eslint
$ npm run format # format with prettier
$ npm run bdd # run bdd/gherkin tests
```

## command line exemples
```bash
$ node ./app/fleet create user2
$ node ./src/app/fleet.js register-vehicle user2 v_123
$ node ./src/app/fleet.js localize-vehicle user2 v_124 13 67.5
$ node ./src/app/fleet.js display-fleet user2
$ node ./src/app/fleet.js display-vehicle-location user2 v_123
```


## Sujets non abordés
- La vérification des inputs, par ex: une location doit être une location (lon,lat valides, etc ...)
- gestion des erreurs
- Des scénarii configurables/dynamiques dans le BDD
- Test unitaires avec Jest
- typescript
- configuration par environnements

## CI/CD ECS
- un exemple d'un deploiment sur ECS :
- docker build
- push docker image into ECR
- aws service update with the new image (montrer exemple actuel) (cette action devrait être séparée, ailleurs)

