require("../../src/infra/logger").configure("bdd");

const assert = require("assert/strict");
const factory = require("../../src/domain/factory");
const Repository = require("../../src/domain/Repository");

const FleetCommandService = require("../../src/domain/FleetCommandService");
const FleetQueryService = require("../../src/domain/FleetQueryService");

const { setWorldConstructor } = require("@cucumber/cucumber");
const Location = require("../../src/domain/values_obj/Location");

class CustomWorld {
	constructor() {
		this.repository = new Repository(factory);
		this.fleetCommandService = new FleetCommandService(this.repository, factory);
		this.fleetQueryService = new FleetQueryService(this.repository);
	}

	randow_id(prefix) {
		return `${prefix}_${Date.now()}`;
	}

	async create_my_fleet() {
		this.my_fleet_id = this.randow_id("my_fleet");
		await this.fleetCommandService.create_user_fleet(this.my_fleet_id);
	}

	async create_other_user_fleet() {
		this.other_user_fleet = this.randow_id("other_user_fleet");
		await this.fleetCommandService.create_user_fleet(this.other_user_fleet);
	}

	async create_a_vehicle() {
		this.my_vehicle_number = this.randow_id("my_vehicle");
		await this.fleetCommandService.create_vehicle(this.my_vehicle_number);
	}

	async register_my_vehicle_into_my_fleet() {
		await this.fleetCommandService.register_existing_vehicle(this.my_fleet_id, this.my_vehicle_number);
	}

	async register_my_vehicle_into_other_user_fleet() {
		await this.fleetCommandService.register_existing_vehicle(this.other_user_fleet, this.my_vehicle_number);
	}

	create_location() {
		this.a_location = new Location({ lat: 12, lon: 45 });
	}

	async park_vehicle_at_this_location() {
		await this.fleetCommandService.localize_vehicle(this.my_fleet_id, this.my_vehicle_number, this.a_location);
	}

	async verify_location() {
		const actual_location = await this.fleetQueryService.get_vehicle_localization(this.my_fleet_id, this.my_vehicle_number);
		assert.ok(actual_location.isEquals(new Location({ lat: 12, lon: 45 })), "Location should be the same");
	}

	async load_my_vehicle() {
		return await this.fleetQueryService.load_vehicle(this.my_fleet_id, this.my_vehicle_number);
	}

	async load_my_fleet() {
		return await this.fleetQueryService.load_user_fleet(this.my_fleet_id);
	}

	async ensure_my_vehicle_is_part_of_my_fleet() {
		const fleet = await this.load_my_fleet();
		fleet.ensure_possesses_this_vehicle(this.my_vehicle_number);
	}
}

setWorldConstructor(CustomWorld);
