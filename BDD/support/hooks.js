const { AfterAll, BeforeAll, Before, After } = require("@cucumber/cucumber");
const logger = require("../../src/infra/logger").getLogger("hooks");

BeforeAll(async function () {
	logger.debug("start BDD");
});

AfterAll(async function () {
	logger.debug("end BDD");
});

Before(async function () {});

After(async function () {
	logger.debug("clean BDD");
	this.repository.clean_data();
});
