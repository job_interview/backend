const { Given, When, Then } = require("@cucumber/cucumber");
const assert = require("assert/strict");

// this can be more dynamic

Given("my fleet", async function () {
	this.fleet_name = "myFleet";
	await this.create_my_fleet(await this.fleetName);
});

Given("a vehicle", async function () {
	await this.create_a_vehicle();
});

When("I register this vehicle into my fleet", async function () {
	await this.register_my_vehicle_into_my_fleet();
});

When("I have registered this vehicle into my fleet", async function () {
	await this.register_my_vehicle_into_my_fleet();
});

When("I try to register this vehicle into my fleet", async function () {
	try {
		await this.register_my_vehicle_into_my_fleet();
	} catch (e) {
		this.lastError = e;
	}
});

Then("this vehicle should be part of my vehicle fleet", async function () {
	await this.ensure_my_vehicle_is_part_of_my_fleet();
});

Then("I should be informed this this vehicle has already been registered into my fleet", async function () {
	assert.strictEqual(await this.lastError?.message, "already_registrered");
});

Given("the fleet of another user", async function () {
	await this.create_other_user_fleet();
});

Given("this vehicle has been registered into the other user's fleet", async function () {
	await this.register_my_vehicle_into_other_user_fleet();
});

Given("a location", async function () {
	await this.create_location();
});

When("I park my vehicle at this location", async function () {
	await this.park_vehicle_at_this_location();
});

Then("the known location of my vehicle should verify this location", async function () {
	await this.verify_location();
});

Given("my vehicle has been parked into this location", async function () {
	await this.park_vehicle_at_this_location();
});

When("I try to park my vehicle at this location", async function () {
	try {
		// this design pattern should be refacored
		await this.park_vehicle_at_this_location();
	} catch (e) {
		this.lastError = e;
	}
});

Then("I should be informed that my vehicle is already parked at this location", async function () {
	assert.strictEqual(await this.lastError?.message, "already_located_in_this_location");
});
