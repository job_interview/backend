module.exports = {
	default: {
		paths: ["BDD/features/*.feature"],
		require: ["BDD/support/**/*.js"],
	},
};
