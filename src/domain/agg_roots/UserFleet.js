// not necessary to modelize all concept of the application, context bounded
// we merge user and fleet notions, this can be improoved et modified in the future if needed
module.exports = class UserFleet {
	#vehicle_numbers = new Set();

	constructor({ user_fleet_id, vehicle_numbers }) {
		this.user_fleet_id = user_fleet_id;
		this.#vehicle_numbers = new Set(vehicle_numbers);
	}

	register_vehicle(vehicle_number) {
		if (this.#vehicle_numbers.has(vehicle_number)) throw new Error("already_registrered");

		this.#vehicle_numbers.add(vehicle_number);
	}

	ensure_possesses_this_vehicle(vehicle_number) {
		if (!this.#vehicle_numbers.has(vehicle_number)) throw new Error(`this fleet doesn't have the vehicle ${vehicle_number}`);
	}

	get vehicle_numbers() {
		return Array.from(this.#vehicle_numbers);
	}

	set vehicle_numbers(p_vehicle_numbers) {
		this.#vehicle_numbers = new Set(p_vehicle_numbers);
	}

	toString() {
		return `UserFleet : ${this.user_fleet_id} - ${this.vehicle_numbers}`;
	}
};
