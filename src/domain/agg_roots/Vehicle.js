module.exports = class Vehicle {
	constructor({ number }) {
		this.number = number;
	}

	park_vehicle(location) {
		if (location.isEquals(this.location)) throw new Error("already_located_in_this_location");
		this.location = location;
	}

	// computeGPSLocation() {
	// 	//may be in the repository
	// 	if (this.location) {
	// 		const { lat, lon } = this.location;
	// 		Object.assign(this, { lat, lon });
	// 	}
	// 	delete this.location;
	// }
};
