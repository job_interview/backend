const assert = require("assert/strict");

module.exports = class FleetCommandService {
	constructor(repository, factory) {
		// injection de dépendance
		this.repository = repository;
		this.factory = factory;
	}

	async create_user_fleet(user_fleet_id) {
		const user_fleet = this.factory.create_user_fleet(user_fleet_id);
		await this.repository.save_user_fleet(user_fleet);
		return user_fleet;
	}

	async create_vehicle(vehicle_number) {
		const vehicle = this.factory.create_vehicle(vehicle_number);
		await this.repository.save_vehicle(vehicle);
		return vehicle;
	}

	async register_existing_vehicle(user_fleet_id, vehicle_number) {
		const user_fleet = await this.repository.load_user_fleet(user_fleet_id); // question CQRS ? passer par la couche query
		assert.ok(user_fleet, "this user_fleet doesn't exist");

		//ensure the vehicle exists:
		const vehicle = await this.repository.load_vehicle(vehicle_number);
		assert.ok(vehicle, "this vehicle doesn't exist");

		//register:
		user_fleet.register_vehicle(vehicle_number);
		//persists:
		await this.repository.save_user_fleet(user_fleet);
	}

	async localize_vehicle(user_fleet_id, vehicle_number, location) {
		const user_fleet = await this.repository.load_user_fleet(user_fleet_id);
		assert.ok(user_fleet, "this user_fleet doesn't exist");

		//vehicle exists
		const vehicle = await this.repository.load_vehicle(vehicle_number);
		assert.ok(vehicle, "this vehicle doesn't exist");

		//ensure that the vehicle belongs to my fleet:
		user_fleet.ensure_possesses_this_vehicle(vehicle_number);
		vehicle.park_vehicle(location);
		await this.repository.save_vehicle(vehicle);
	}
};
