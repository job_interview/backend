const UserFleet = require("./agg_roots/UserFleet");
const Vehicle = require("./agg_roots/Vehicle");
const Location = require("./values_obj/Location");

module.exports = {
	create_user_fleet: (user_fleet_id) => {
		return new UserFleet({
			user_fleet_id,
			vehicle_numbers: [],
		});
	},

	create_vehicle: (number) =>
		new Vehicle({
			number,
		}),

	create_vehicle_from_db: ({ number, location }) => {
		const vehicle = new Vehicle({
			number,
		});
		if (location) vehicle.location = new Location(location);
		return vehicle;
	},

	create_fleet_from_db: ({ user_fleet_id, vehicle_numbers }) => {
		const fleet = new UserFleet({
			user_fleet_id,
			vehicle_numbers,
		});
		return fleet;
	},
};
