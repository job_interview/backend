//we can for example compute the distance betueen 2 locations
module.exports = class {
	constructor({ lat, lon, alt }) {
		Object.assign(this, { lat, lon, alt });
	}
	isEquals(location) {
		return location && location.lat === this.lat && location.lon === this.lon && location.alt === this.alt;
	}
};
