const DiskDataBase = require("../infra/DiskDataBase");

const assert = require("assert");

module.exports = class Repository {
	constructor(factory, dbToUse) {
		this.db = dbToUse || new DiskDataBase();
		this.factory = factory;
	}

	async load_user_fleet(user_fleet_id) {
		const user_fleet_conf = await this.db.load_user_fleet(user_fleet_id);
		assert.ok(user_fleet_conf, "this user_fleet doesn't exist");
		const user_fleet = this.factory.create_fleet_from_db(user_fleet_conf);
		return user_fleet;
	}

	async load_vehicle(vehicle_number) {
		const vehicle_conf = await this.db.load_vehicle(vehicle_number);
		assert.ok(vehicle_conf, "this vehicle doesn't exist");
		const vehicle = this.factory.create_vehicle_from_db(vehicle_conf);
		return vehicle;
	}

	async vehicle_exists(vehicle_number) {
		const vehicle = await this.db.load_vehicle(vehicle_number);
		return !!vehicle;
	}

	async save_user_fleet(user_fleet) {
		const { user_fleet_id, vehicle_numbers } = user_fleet;
		await this.db.save_user_fleet({ user_fleet_id, vehicle_numbers });
	}

	async save_vehicle(vehicle) {
		await this.db.save_vehicle(vehicle);
	}

	async clean_data() {
		await this.db.clean_data();
	}
};
