module.exports = class FleetQueryService {
	constructor(repository) {
		this.repository = repository;
	}

	async load_user_fleet(user_fleet_id) {
		return this.repository.load_user_fleet(user_fleet_id);
	}

	async load_vehicle(user_fleet_id, vehicle_number) {
		const user_fleet = await this.repository.load_user_fleet(user_fleet_id);

		//vehicle blongs to the fleet
		user_fleet.ensure_possesses_this_vehicle(vehicle_number);

		const vehicle = await this.repository.load_vehicle(vehicle_number);

		return vehicle;
	}

	async get_vehicle_localization(user_fleet_id, vehicle_number) {
		const { location } = await this.load_vehicle(user_fleet_id, vehicle_number);
		return location;
	}
};
