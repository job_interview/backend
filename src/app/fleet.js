require("../infra/logger").configure("local");

const SurrealDB = require("../infra/SurrealDB");
const factory = require("../domain/factory");
const Repository = require("../domain/Repository");
const FleetCommandService = require("../domain/FleetCommandService");
const FleetQueryService = require("../domain/FleetQueryService");

// some libs manage dependancies injections

async function main() {
	const surrealdb = new SurrealDB();
	await surrealdb.initDB();
	const repository = new Repository(factory, surrealdb);
	const fleetCommandService = new FleetCommandService(repository, factory);
	const fleetQueryService = new FleetQueryService(repository);

	await require("./queries")(fleetQueryService);
	await require("./commands")(fleetCommandService);

	await surrealdb.close();
}

main();
