const logger = require("../infra/logger").getLogger("command");
const Location = require("../domain/values_obj/Location");

// eslint-disable-next-line no-undef
const argv = process.argv.slice(2);

module.exports = async (fleetCommandService) => {
	if (argv[0] === "create" && argv[1]) {
		logger.debug(argv);
		const { user_fleet_id } = await fleetCommandService.create_user_fleet(argv[1]);
		logger.info(`UserFleet created : user_fleet_id : ${user_fleet_id}`);
	}

	if (argv[0] === "register-vehicle" && argv[1] && argv[2]) {
		logger.debug(argv);
		const vehicle = await fleetCommandService.create_vehicle(argv[2]);
		logger.info("vehicle created", vehicle);
		await fleetCommandService.register_existing_vehicle(argv[1], argv[2]);
		logger.info("vehicle registred");
	}

	if (argv[0] === "localize-vehicle" && argv[1] && argv[2] && argv[3] && argv[4]) {
		logger.debug(argv);
		const location = new Location({ lat: argv[3], lon: argv[4], alt: argv[5] });
		await fleetCommandService.localize_vehicle(argv[1], argv[2], location);
		logger.info("vehicle parked");
	}
};
