// eslint-disable-next-line no-undef
const argv = process.argv.slice(2);
const logger = require("../infra/logger").getLogger("queries");

module.exports = async (fleetQueryService) => {
	if (argv[0] === "display-fleet" && argv[1]) {
		logger.debug(argv);
		const userFleet = await fleetQueryService.load_user_fleet(argv[1]);
		logger.debug(userFleet.toString());
	}

	if (argv[0] === "display-vehicle-location" && argv[1] && argv[2]) {
		logger.debug(argv);
		const location = await fleetQueryService.get_vehicle_localization(argv[1], argv[2]);
		logger.debug(location);
	}
};
