const JSONdb = require("simple-json-db");
const db = new JSONdb("/tmp/full_test_database.json");

const assert = require("assert");
const logger = require("../infra/logger").getLogger("DiskDataBase");

module.exports = class DiskDataBase {
	async load_user_fleet(fleet_id) {
		return db.get(`user_fleet_${fleet_id}`);
	}

	async load_vehicle(vehicle_number) {
		const vehicle = db.get(`vehicle_${vehicle_number}`);
		logger.debug("load_vehicle", vehicle);
		return vehicle;
	}

	async save_user_fleet(user_fleet) {
		logger.debug("save", user_fleet);
		assert.ok(user_fleet && user_fleet.user_fleet_id, "all fleet must have an identification");
		db.set(`user_fleet_${user_fleet.user_fleet_id}`, user_fleet);
		return user_fleet;
	}

	async save_vehicle(vehicle) {
		assert.ok(vehicle && vehicle.number, "all vehicle must have an number");
		logger.debug("save_vehicle", vehicle);
		db.set(`vehicle_${vehicle.number}`, vehicle);
	}

	async clean_data() {
		db.JSON({ data: {} });
	}
};
