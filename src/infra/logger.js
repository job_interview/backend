const log4js = require("log4js");

module.exports = {
	getLogger: (name) => log4js.getLogger(name),
	configure: (env = "local") => {
		log4js.configure(require(`./log4js-${env}.json`));
	},
};
