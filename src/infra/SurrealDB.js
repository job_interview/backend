const assert = require("assert");
const { default: Surreal, RecordId } = require("surrealdb");
const logger = require("../infra/logger").getLogger("SurrealDataBase");

module.exports = class SurrealDB {
	async initDB() {
		this.db = new Surreal();
		// eslint-disable-next-line no-undef
		await this.db.connect(process.env["SURREAL_DB_URL"], {
			namespace: "manu-namespace",
			database: "manu-database",
			auth: {
				username: "admin",
				// eslint-disable-next-line no-undef
				password: process.env["SURREAL_DB_PASS"], // this should be stored in a more secure way
			},
		});
	}

	async close() {
		await this.db.close();
	}

	async load_user_fleet(fleet_id) {
		return this.db.select(new RecordId("full_user_fleets", fleet_id));
	}

	async load_vehicle(vehicle_number) {
		const vehicle = await this.db.select(new RecordId("full_vehicles", vehicle_number));
		logger.debug("load_vehicle", vehicle);
		return vehicle;
	}

	async save_user_fleet(user_fleet) {
		logger.debug("save", user_fleet);
		assert.ok(user_fleet && user_fleet.user_fleet_id, "all fleet must have an identification");
		await this.db.upsert(new RecordId("full_user_fleets", user_fleet.user_fleet_id), user_fleet);
		return user_fleet;
	}

	async save_vehicle(vehicle) {
		assert.ok(vehicle && vehicle.number, "all vehicle must have an number");
		logger.debug("save_vehicle", vehicle);
		await this.db.upsert(new RecordId("full_vehicles", vehicle.number), vehicle);
	}

	async clean_data() {
		throw new Error("Not Authorized");
	}
};
